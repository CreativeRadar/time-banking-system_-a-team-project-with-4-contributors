
variable "flavor" { default = "m1.small" }
variable "image" { default = "Debian Stretch 9.8.0" }

#variable "instance" { default = "tf_instance" }

variable "name" { default = "server" }
variable "name2" { default = "MHA Systems Test" }




variable "keypair" { default = "tmpKey" } # you need to change this
variable "pool" { default = "cscloud_private_floating" }
variable "server_script" { default = "./server.sh" }
variable "security_description" { default = "Terraform security group" }
variable "security_name" { default = "tf_securityTest3" }
