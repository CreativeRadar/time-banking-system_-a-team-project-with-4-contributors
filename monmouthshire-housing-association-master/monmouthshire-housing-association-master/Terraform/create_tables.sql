SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE IF NOT EXISTS `CMT652_1819_TEAM_1`.`volunteers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `is_mha_tenant` tinyint(4) NOT NULL,
  `emergency_contact_number` varchar(20) NOT NULL,
  `previously_volunteered` tinyint(4) NOT NULL,
  `previously_volunteered_who_for_and_role` varchar(300) DEFAULT NULL,
  `disability_or_support_needs` tinyint(4) NOT NULL,
  `disability_or_support_needs_details` varchar(300) DEFAULT NULL,
  `medical_conditions_or_medications` tinyint(4) NOT NULL,
  `medical_conditions_or_medications_details` varchar(300) DEFAULT NULL,
  `known_allergies` tinyint(4) NOT NULL,
  `known_allergies_details` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `CMT652_1819_TEAM_1`.`businesses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `main_contact_name` varchar(45) NOT NULL,
  `main_contact_number` varchar(45) NOT NULL,
  `about_business` varchar(300) NOT NULL,
  `find_out_about_time_exchange` varchar(300) NOT NULL,
  `why_join_time_exchange` varchar(300) NOT NULL,
  `organisation_offers` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `CMT652_1819_TEAM_1`.`users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  `role` varchar(20) NOT NULL,
  `business_id` int(11) DEFAULT NULL,
  `time_credits` varchar(45) NOT NULL DEFAULT '0',
  `profile_image` varchar(45) DEFAULT '/image/stock.png',
  `barcode_image` varchar(45) DEFAULT NULL,
  `address_line_one` varchar(200) NOT NULL,
  `address_line_two` varchar(200) DEFAULT NULL,
  `address_line_three` varchar(200) DEFAULT NULL,
  `postcode` varchar(10) NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `volunteer_id` int(11) DEFAULT NULL,
  `password` varchar(45) DEFAULT 'hello',
  `salt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `business_id_idx` (`business_id`),
  KEY `volunteer_id_idx` (`volunteer_id`),
  CONSTRAINT `business_id` FOREIGN KEY (`business_id`) REFERENCES `businesses` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `volunteer_id` FOREIGN KEY (`volunteer_id`) REFERENCES `volunteers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `CMT652_1819_TEAM_1`.`offers` (
  `offer_id` int(11) NOT NULL AUTO_INCREMENT,
  `offer` varchar(100) DEFAULT NULL,
  `business_name` varchar(100) DEFAULT NULL,
  `credits` int(11) DEFAULT NULL,
  PRIMARY KEY (`offer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `CMT652_1819_TEAM_1`.`transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '0 = spend (offer id)\n1 = earn (task and credits)',
  `offer_id` int(11) DEFAULT NULL,
  `task_description` varchar(200) DEFAULT NULL,
  `credits_earned` int(11) DEFAULT NULL,
  `timestamp` DATE,
  `location` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`),
  KEY `offer_id_idx` (`offer_id`),
  CONSTRAINT `offer_id` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`offer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
SET FOREIGN_KEY_CHECKS = 1;
