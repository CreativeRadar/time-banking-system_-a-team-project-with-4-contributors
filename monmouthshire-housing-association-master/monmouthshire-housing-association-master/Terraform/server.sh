#!/bin/bash

echo "cd to root directory..."
cd root

echo "updating..."
sudo apt-get update -y

echo "upgrading..."
sudo apt-get upgrade -y

echo "installing gfortran..."
sudo apt-get install gcc-gfortran -y

echo "installing git..."
sudo apt-get install git -y

echo "installing gitlab server key..."
touch .ssh/known_hosts
ssh-keyscan gitlab.cs.cf.ac.uk >> .ssh/known_hosts
chmod 644 .ssh/known_hosts

echo "installing gitlab deployment key..."
touch C1753005_keypair.key
cat << `EOF` >> C1753005_keypair.key
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAs9SZuAi4deZVEiQJDyoE3sABeBe6zR7ZEvE+jxC9sBWFbWwn
xlcZP5hqzxQLvyN5U1nt3a6IpHJiaWcmv2VrRQavbvygoPCFAbgrlAFL1nqnp8Lw
daaXEgSXzJm1Urdm05xRmCJlChKbv/XMrgeDflncbxSzi3d+c3YlhmMuaz/DIr4Z
rhArO7LHziZBN7m1+gApzRfsoRg4mlf4T10QtwiuAy8wQFrQnwuUjc/XtZj8oFfG
5o6YPWgksbk7CINdsSGyR23d04D8m89Eha9AVXkbV5ZyA5lm8g3x8k4mvmPVLzdY
5vaFjMXGEPCantauuXEJXDbMnUDjLmJeO6l/WQIDAQABAoIBADIX9voc88Vj9C6p
exyOR+0E72PUuGSLQ0zThumzcCOs2w4wIqRNfusIOjiVDPChoRpm4qvuAoJMmr1h
zpliVw9TXWs9VEGAvoxMEVeZRNbqM+m3k+QyE8MhJGgKKUhxbmNkGBaY/ixgIqLG
hc20qhx4S79ltN4U21SuhKV622NkV8NnbzvmBvDD3PQ/YUeCHfJXKmMSBnw66Y1X
7gZrS0TVlQxcKHvG46roJibAxlHE4H864apxWTLA9Omi9AGzWet+hrynDmt8pBGn
8mE/pxfR9S9A8ES/mGtvCk6DijAk2c4Wp7DnVjht9V6noBYL3yyNMqTQyv6GN7NU
SRmtD0ECgYEA56oveXwcPtVS/ZbDvbDfo8rmwc6JM52VdovcnnNx1S5kPZNcrkHo
EsEkQaQt4pwxF0Jt7+V17Balma90fT/cBM1CkgGRbh2OtD91toURflWjpbo0IUer
/nAv7K+AMXMTvPTXLDdpKZg91cZdZhNM+AUKZlflkIIR6zkKBYAoUcsCgYEAxriD
JWvD3p5QVzpaW/EnLQuT9IcFgH+QlmoAdtNDKOjiADoup1n/bRIDzafL9GP2VfC2
cqN+e6GRv5YDtWrEIJc+rJGAvYeSfDZ88pz4SLzYpH1wO5BnDbWXQ+SaAggEWCF2
+ovkUBEw6oFcs8rdjYchUcTGIaQHZKQucMSd/usCgYA6CRZvlrn0u7hYswZCZScB
f3kjoDwa5CVnV05jm4Qu5bpS+rHDLduXpy8QCrM8C+j4bLnh2VgbVukRgTTrk63o
vgspTJ9EOL9hRGyW1us0/PtML0OT+sKJsU0wPB8fRDbzpVqqRw9fTjZfcZxAUCF+
GLUsQmTOOOeIR/fMvPhc1QKBgAFNyJ6APILDayeHXHxp8qXbA5eBI0oajCpQe7xL
2o70KDd0KkYy0vg2PAwnY7Ewzrb1o1zRMc1B3CDzrQaNsse79k9PjRaxQwbJnLxC
x2ZCxvEdWZUqd2KhpGJvp4sH3m9tmMhADrlvw3BmdONBG38IOfG9Zsdluh6w7RUU
VS0NAoGAJ9sx28sqX4BhzVsxAHifiTNy9S4sttZK0/gBCWYnDjUtEja1pkRlxsVF
xtR37GKD2d2WYYp6G4hsIkuEf5XDcGLT7maeYElSxhm/7oD9KX5A0pYEOR3Rwv0a
yo71M5E1bOZ3RbqK7SnsFJ0Qd0SwO0WEmm+YkNzGsmZrh0qaFqk=
-----END RSA PRIVATE KEY-----
`EOF`
chmod 400 C1753005_keypair.key

echo "cloning repository..."
ssh-agent bash -c 'ssh-add C1753005_keypair.key; git clone git@gitlab.cs.cf.ac.uk:c1753005/monmouthshire-housing-association.git'

echo "changing to repository directory..."
cd monmouthshire-housing-association

echo "enabling execution..."
chmod a+x *.sh

echo "compiling matrix..."
./make.sh

echo "running matrix..."
nohup ./run.sh &


sudo apt-get update
sudo apt-get install wget -y
sudo apt-get install curl -y
sudo apt-get install zip -y
sudo apt-get install unzip -y
zip -v
unzip -v



echo "Installing Java 10..."
curl -O https://download.java.net/java/GA/jdk10/10.0.2/19aef61b38124481863b1413dce1855f/13/openjdk-10.0.2_linux-x64_bin.tar.gz
tar zxvf openjdk-10.0.2_linux-x64_bin.tar.gz
sudo mv jdk-10.0.2 /usr/local/
export JAVA_HOME=/usr/local/jdk-10.0.2
export PATH=$PATH:$JAVA_HOME/bin
java -version
sudo ln -s /usr/local/jdk-10.0.2/bin/java /usr/bin/java




sudo apt-get update
sudo apt update

echo "install java 8"
sudo apt install openjdk-8-jdk -y
sudo apt-get install openjdk-8-jdk -y
sudo update-alternatives --config java
java -version


echo "installing MariaDB..."
sudo apt-get update
sudo apt-get install mysql-server -y
sudo apt-get install mariadb-server -y
sudo systemctl start mariadb
sudo systemctl status mariadb
sudo systemctl enable mariadb

#set root password
sudo /usr/bin/mysqladmin -u root password 'comsc'

#SET UP SCEHMA
mysql -u root -pcomsc -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY 'comsc' WITH GRANT OPTION; FLUSH PRIVILEGES;"

# restart
sudo /etc/init.d/mysql restart

mysql -u root -pcomsc -e "CREATE DATABASE IF NOT EXISTS CMT652_1819_TEAM_1;"
mysql -uroot -proot CMT652_1819_TEAM_1 < create_tables.sql




echo "Installing gradle..."
wget https://services.gradle.org/distributions/gradle-4.7-bin.zip
sudo mkdir /opt/gradle
unzip -d /opt/gradle gradle-4.7-bin.zip
export PATH=$PATH:/opt/gradle/gradle-4.7/bin
gradle -v


echo "gradle bootrun executed as a background process"
gradle bootrun &