DROP TABLE IF EXISTS businesses;

CREATE TABLE IF NOT EXISTS businesses(
  id long NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name varchar(100) DEFAULT NULL,
  summary varchar(100) DEFAULT NULL,
  address varchar(100) DEFAULT NULL,
  logoURL varchar(100) DEFAULT NULL,
);

DROP TABLE IF EXISTS offers;
CREATE TABLE IF NOT EXISTS offers(
  offer_id long NOT NULL AUTO_INCREMENT PRIMARY KEY,
  offer varchar(100) DEFAULT NULL,
  credits int(11) DEFAULT NULL,
  business_id int(11) DEFAULT NULL,
  business_name varchar(100) DEFAULT NULL,
);

DROP TABLE IF EXISTS volunteers;
CREATE TABLE IF NOT EXISTS volunteers(
  id long NOT NULL AUTO_INCREMENT PRIMARY KEY,
  firstname varchar(45) NOT NULL,
  lastname varchar(45) NOT NULL,
  phone_number  varchar(45) NOT NULL,
  time_credits long,
  earned_time_credits long,
  email varchar(45) NOT NULL,
  postcode varchar(45) NOT NULL,
  profile_image varchar(45) NOT NULL,
  barcode_image varchar(45) NOT NULL
  );

  DROP TABLE IF EXISTS users;
  CREATE TABLE IF NOT EXISTS users(
   id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
   email varchar(200) NOT NULL,
   role varchar(20) NOT NULL,
   business_id int(11) DEFAULT NULL,
   time_credits varchar(45) NOT NULL,
   profile_image varchar(45) DEFAULT,
   barcode_image varchar(45) DEFAULT NULL,
   address_line_one varchar(200) NOT NULL,
   address_line_two varchar(200) DEFAULT NULL,
   address_line_three varchar(200) DEFAULT NULL,
   postcode varchar(10) NOT NULL,
   phone_number varchar(20) DEFAULT NULL,
   volunteer_id int(11) DEFAULT NULL,
   password varchar(45) DEFAULT,
   salt varchar(45) DEFAULT NULL
    );