INSERT INTO businesses (id,name,summary,address,logoURL)
VALUES(1,'MDN','mozilla','2 devon place','EF/RG/TG');

INSERT INTO offers (offer_id,offer,credits,business_id,business_name)
VALUES(1,'2 vidoes','2',1,'MDN');


INSERT INTO volunteers(id,firstname,lastname,phone_number,time_credits,earned_time_credits,email,postcode,profile_image,barcode_image)
VALUES (1,'ERIC','MOORE','1333344',22,23,'E@GMAIL.COM','NP19DHD','img/profile.png','img/barcode.png');

INSERT INTO users(id,email,role,business_id,time_credits,profile_image,barcode_image,address_line_one,address_line_two,address_line_three,postcode,phone_number,volunteer_id,password)
VALUES(1,'u@cardiff.ac.uk','volunteer',1,0,'/image/stock.png','/image/stock.jpg','2 devon place','gwent','UK','np10293c','654321345',1,'hello')