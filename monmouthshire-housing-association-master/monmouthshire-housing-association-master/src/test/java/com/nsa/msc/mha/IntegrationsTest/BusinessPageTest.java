package com.nsa.msc.mha.IntegrationsTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc

public class BusinessPageTest {
    @Autowired
    private MockMvc mockMvc;

    //    Component test using a mock MVC
    @Test
    public void LoadBusinessPage() throws Exception {
        this.mockMvc.perform(get("/businesses/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("mozilla")));


    }

}