package com.nsa.msc.mha;

        import com.nsa.msc.mha.Forms.BusinessForm;
        import org.junit.Before;
        import org.junit.Test;

        import static org.junit.Assert.assertEquals;
        import static org.junit.Assert.assertNotNull;

public class BusinessFormTest { //(Knight, L. 2018)

    private BusinessForm bf1;

    @Before
    public final void createBusinessForm(){
       bf1 = new BusinessForm("Test business","test-email@gmail.com","4 Wood Street",
                "Brackla","Bridgend", "CF4 5TR","Bob Smith",
                "01258562358","Test","Facebook",
                "We really like the idea that volunteers can earn time credits",
                "20% off 2 adult cinema tickets with 5 time credits");
    }

    @Test
    public void businessFormClassExists(){
        assertNotNull(bf1);
    }

    @Test
    public void getNameReturnsCorrectName(){
        assertEquals("Test business",bf1.getName());
    }

}

/* References:
Knight, L. 2018. Week 4: Collections, testing. Avaliable at:
https://learningcentral.cf.ac.uk/webapps/blackboard/content/listContent.jsp?course_id=_387592_1&content_id=_4846541_1
 [Accessed: 09 December 2018]. */
