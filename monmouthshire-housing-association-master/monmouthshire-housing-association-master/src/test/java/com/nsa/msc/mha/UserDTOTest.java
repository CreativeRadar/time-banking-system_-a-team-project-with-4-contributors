package com.nsa.msc.mha;

import com.nsa.msc.mha.dto.UserDTO;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserDTOTest {

    private UserDTO userOne;

    private static final String PASS_ONE = "myPa$$w0rd";
    private static final String PASS_TWO = "0th3rp#ssw0rd";

    private static final String ENCRYPTED_PASS = "$2a$10$yWh5ulOG7s31GPotxODGA.ZGHFWJEVZZ9ZhPwYE5cyVKARSeHM7Hq";
    private static final int USER_ID = 123;
    private static final String ROLE = "volunteer";

    @Before
    public final void before() {
        userOne = new UserDTO();
    }

    @Test
    public void emptyConstructorIsSane() {
        assertNotNull(userOne);
        assertNull(userOne.getPassword());
    }

    @Test
    public void idAndPasswordConstructorIsSane() {
        UserDTO userTwo = new UserDTO(USER_ID, ENCRYPTED_PASS, ROLE);
        assertNotNull(userTwo);
        assertEquals(userTwo.getId(), USER_ID);
        assertEquals(userTwo.getPassword(), ENCRYPTED_PASS);
        assertEquals(userTwo.getRole(), ROLE);
    }

    @Test
    public void passwordIsGenerated() {
        userOne.setEncryptedPassword(PASS_ONE);
        assertNotNull(userOne.getPassword());
        System.out.println(userOne.getPassword());
    }

    @Test
    public void correctPasswordIsValid() {
        userOne.setEncryptedPassword(PASS_ONE);
        assert(userOne.verifyPassword(PASS_ONE));
    }

    @Test
    public void incorrectPasswordIsNotValid() {
        userOne.setEncryptedPassword(PASS_TWO);
        assertFalse(userOne.verifyPassword(PASS_ONE));
    }
}
