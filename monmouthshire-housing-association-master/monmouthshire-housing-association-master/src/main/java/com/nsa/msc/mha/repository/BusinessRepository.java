package com.nsa.msc.mha.repository;

import com.nsa.msc.mha.Forms.BusinessForm;
import com.nsa.msc.mha.dto.BusinessDTO;
import java.util.List;

public interface BusinessRepository {
    public List<BusinessDTO> findAll();
    public boolean delete(long id);
    public long addBusinessToBusinessesTable (BusinessForm business);
    public int addBusinessToUsersTable (BusinessForm business);
}

