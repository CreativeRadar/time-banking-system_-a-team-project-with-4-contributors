package com.nsa.msc.mha.dto;

// Comment out the lombok annotations or the build fails
// import lombok.AllArgsConstructor;
// import lombok.Data;

//@Data
//@AllArgsConstructor
public class BusinessDTO {

    private long id;
    private String name;
    private String summary;
    private String address;
    private String logoUrl;

    // Summary constructor for dashboard
    public BusinessDTO(long id, String name,String summary, String address, String logoUrl) {
        this.id = id;
        this.name = name;
        this.summary = summary;
        this.address = address;
        this.logoUrl = logoUrl;
    }

    //Added to fix errors when running the testing class BusinessFormTest
    public BusinessDTO(long id, String name, String summary) {
        this.id = id;
        this.name = name;
        this.summary = summary;
    }

    //Added to fix errors when running the testing class BusinessFormTest
    public String getName() {
        return name;
    }
    public String getSummary() {
        return summary;
    }
    public String getLogoUrl() {
        return logoUrl;
    }
    public long getId() { return id; }

    public String getUrl() {
        return "/businesses/" + id;
    }

}