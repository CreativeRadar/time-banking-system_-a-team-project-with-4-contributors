package com.nsa.msc.mha.Forms;

public class AddCreditsForm {
    private long id;
    private int credits;
    private String description;
    private String location;

    public AddCreditsForm(long id, int credits, String description, String location) {
        this.id = id;
        this.credits = credits;
        this.description = description;
        this.location = location;
    }

    public long getId() {
        return id;
    }

    public int getCredits() {
        return credits;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }
}
