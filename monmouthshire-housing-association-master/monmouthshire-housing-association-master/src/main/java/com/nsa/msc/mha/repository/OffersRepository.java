package com.nsa.msc.mha.repository;

import com.nsa.msc.mha.Forms.AddOffersForm;
import com.nsa.msc.mha.dto.OfferDTO;

import java.util.List;

public interface OffersRepository {

    public List<OfferDTO> listAllOffers();

    public int addOffer(AddOffersForm addOffersForm);

    public int deleteOffer(int offer_id);

}