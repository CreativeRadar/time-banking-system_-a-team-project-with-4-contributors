package com.nsa.msc.mha.repository;

import com.nsa.msc.mha.dto.BusinessDTO;

import java.util.List;

public interface BusinessRepositoryInterface {
    public List<BusinessDTO> findAll();
    public Object findById(Long id);
    public Object findByname(String business_name);




}

