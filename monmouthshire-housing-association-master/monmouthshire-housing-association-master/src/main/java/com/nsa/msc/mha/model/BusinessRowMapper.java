package com.nsa.msc.mha.model;

import com.nsa.msc.mha.dto.BusinessDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BusinessRowMapper implements RowMapper<BusinessDTO> {
    @Override
    public BusinessDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new BusinessDTO(
                rs.getLong("id"),
                rs.getString("name"),
                rs.getString("summary"),
                rs.getString("address"),
                rs.getString("logoUrl")
        );

    }
}


