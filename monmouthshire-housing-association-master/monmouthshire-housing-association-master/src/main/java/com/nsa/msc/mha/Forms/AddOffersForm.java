package com.nsa.msc.mha.Forms;

public class AddOffersForm {
    private String offer;
    private String business_name;

    public AddOffersForm(String offer, String business_name){
        this.offer=offer;
        this.business_name=business_name;
    }

    public String getOffer(){
        return offer;
    }

    public  String getBusiness_name(){
        return business_name;
    }
}
