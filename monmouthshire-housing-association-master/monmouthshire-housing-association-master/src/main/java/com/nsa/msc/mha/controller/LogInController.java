package com.nsa.msc.mha.controller;
import com.nsa.msc.mha.Forms.logInForm;
import com.nsa.msc.mha.dto.BusinessDTO;
import com.nsa.msc.mha.dto.UserDTO;
import com.nsa.msc.mha.dto.VolunteerDTO;
import com.nsa.msc.mha.repository.UserRepository;
import com.nsa.msc.mha.repository.VolunteerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import java.util.Map;


@Controller
public class LogInController {

    private UserRepository userRepo;

    @Autowired
    public LogInController(UserRepository uRepo) {
        userRepo = uRepo;
    }

    //@GetMapping(path={"/login"})
    //public String LogInPage(){
      //  return "login";
    //}

    @GetMapping(path={"/login"})
    public ModelAndView LogInPage() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("login");
        Map model = mv.getModel();
        return mv;
    }

    @RequestMapping(path="/loginFormlink", method = RequestMethod.POST)
    public ModelAndView login(logInForm login){
        ModelAndView mv = new ModelAndView();
        Map model = mv.getModel();
        UserDTO userLogin;
        // BusinessDTO businessLogin;
        String emailForLogIn = login.getEmail();
        try {
            userLogin = userRepo.findByEmail(emailForLogIn);

            System.out.println("Sent pw: " + login.getPassword());
            System.out.println("Stored pw: " + userLogin.getPassword());
            if (!login.getPassword().equals(userLogin.getPassword())) {
                model.put("passwordWrong", Boolean.TRUE);
                return new ModelAndView("login", model);
            } else {
                if (userLogin.getRole().equals("volunteer")) {
                    return new ModelAndView("redirect:/volunteerHome/" + userLogin.getId());
                }
                if (userLogin.getRole().equals("business")) {
                    return new ModelAndView("redirect:/businesses/" + userLogin.getId());
                }
                if (userLogin.getRole().equals("admin")) {
                    return new ModelAndView("redirect:/admin");
                }
            }
        }

        catch(Exception e) {
            model.put("passwordWrong", Boolean.TRUE);
            e.printStackTrace();
            return new ModelAndView("login", model);
        }

        return mv;
    }

}
