package com.nsa.msc.mha.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import com.nsa.msc.mha.Forms.AddOffersForm;
import com.nsa.msc.mha.repository.BusinessRepositoryInterface;
import com.nsa.msc.mha.dto.BusinessDTO;
import com.nsa.msc.mha.dto.OfferDTO;
import com.nsa.msc.mha.repository.OffersRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import java.util.Map;

import com.nsa.msc.mha.Forms.BusinessForm;
import com.nsa.msc.mha.repository.BusinessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class BusinessController {

    private BusinessRepository businessRepo;
    private BusinessRepositoryInterface businessRepoInterface;
    private OffersRepository offersRepo;

    @Autowired
    public BusinessController(BusinessRepository businessRepository,
                              BusinessRepositoryInterface businessRepoInterface,
                              OffersRepository offersRepo){

        this.businessRepo = businessRepository;
        this.businessRepoInterface = businessRepoInterface;
        this.offersRepo = offersRepo;
    }


    // Business Registration
    // =====================

    @PostMapping(path={"/registrationConfirmationBusiness"})
    public String registrationConfirmation (BusinessForm businessForm){
        businessRepo.addBusinessToBusinessesTable(businessForm);
        businessRepo.addBusinessToUsersTable(businessForm);

        return "RegistrationConfirmation";
    }

    @GetMapping(path={"/businessRegistration"})
    public String businessRegistration(){
        return "RegistrationForm";
    }



    // Business Profile Page
    // =====================

    @GetMapping("/businesses/{id}")
    public ModelAndView getAllBusinesses(@PathVariable Long id){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("business");
        Map model  = mv.getModel();
        BusinessDTO business;
        business = (BusinessDTO)businessRepoInterface.findById(id);
        model.put("business", business);

        // List all Offers from offers DTO
        List<OfferDTO> offers;
        offers = offersRepo.listAllOffers();
        model.put("offers", offers);

        return mv;
    }

    @PostMapping(path={"/add_offer"})
    public String addNewOffer(AddOffersForm addOffersForm){
        offersRepo.addOffer(addOffersForm);

        return "redirect:/businesses/{business_name}";
    }


}