package com.nsa.msc.mha.repository;
import com.nsa.msc.mha.model.BusinessRowMapper;
import com.nsa.msc.mha.dto.BusinessDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BusinessRepositoryInterfaceJDBC implements BusinessRepositoryInterface {

private JdbcTemplate jdbcTemplate;

@Autowired
    public BusinessRepositoryInterfaceJDBC(JdbcTemplate Template){ this.jdbcTemplate = Template; }

    @Override
    public List<BusinessDTO> findAll() {
        BusinessRowMapper brm = new BusinessRowMapper();
        System.out.println("list");
        return jdbcTemplate.query("select business_id, business_name, business_profile, address, logo_url from business_page", brm);

    }

    @Override
    public BusinessDTO findById(Long id) {
        BusinessRowMapper brm = new BusinessRowMapper();
        return jdbcTemplate.queryForObject("select id, name, summary,address, logoURL from businesses where id = ?",
                new Object[]{id},
                brm  );
    }

    @Override
    public BusinessDTO findByname(String business_name) {
        BusinessRowMapper brm = new BusinessRowMapper();
        return jdbcTemplate.queryForObject("select business_id, business_name, business_profile, address, logo_url from business_page where business_name = ?",
                new Object[]{business_name},
                brm  );
    }


}


