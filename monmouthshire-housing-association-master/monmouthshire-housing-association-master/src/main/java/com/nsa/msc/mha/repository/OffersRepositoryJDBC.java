package com.nsa.msc.mha.repository;

import com.nsa.msc.mha.Forms.AddOffersForm;
import com.nsa.msc.mha.model.OffersRowMapper;
import com.nsa.msc.mha.dto.OfferDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


    @Repository
    public class OffersRepositoryJDBC implements OffersRepository {

        private JdbcTemplate jdbcTemplate2;


        @Autowired
        public OffersRepositoryJDBC(JdbcTemplate Template) {
            this.jdbcTemplate2 = Template;
        }


        @Override
        public List<OfferDTO> listAllOffers() {
            OffersRowMapper orm = new OffersRowMapper();
            return jdbcTemplate2.query("select offer_id, offer, business_name  from offers", orm);

        }



         @Override
           public int addOffer(AddOffersForm addOffersForm) {
            return  jdbcTemplate2.update("insert into offers (offer,business_name) values(?,?);",
                       new Object[] {
                               addOffersForm.getOffer(),
                               addOffersForm.getBusiness_name(),
                       });

           }

           @Override
           public int deleteOffer(int offer_id) {
               return jdbcTemplate2.update("delete from offers where offer_id =(?);",
                       new Object[] {offer_id});
           }

        }


