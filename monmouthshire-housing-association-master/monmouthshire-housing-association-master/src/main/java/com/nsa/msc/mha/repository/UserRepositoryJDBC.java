package com.nsa.msc.mha.repository;

import com.nsa.msc.mha.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryJDBC implements UserRepository {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public UserRepositoryJDBC(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public UserDTO findByEmail(String email) {
        System.out.println("Email sent: " + email);
        return jdbcTemplate.queryForObject("SELECT id, password, role FROM users WHERE email = ?",
                new Object[]{email},
                (rs, i) -> new UserDTO(
                        rs.getLong("id"),
                        rs.getString("password"),
                        rs.getString("role")
                ));

    }
}
