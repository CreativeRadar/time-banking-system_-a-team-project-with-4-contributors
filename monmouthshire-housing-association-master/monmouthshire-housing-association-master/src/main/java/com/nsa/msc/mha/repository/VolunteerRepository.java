package com.nsa.msc.mha.repository;

import com.nsa.msc.mha.Forms.SpendCreditsForm;
import com.nsa.msc.mha.Forms.logInForm;
import com.nsa.msc.mha.Forms.AddCreditsForm;
import com.nsa.msc.mha.dto.VolunteerDTO;
import com.nsa.msc.mha.dto.VolunteerLogDTO;
import com.nsa.msc.mha.Forms.VolunteerForm;

import java.util.List;

public interface VolunteerRepository  {
    //public List<VolunteerDTO> findAll();
    public Object findById(Long id);
    public Object findByEmail(String email);
    // public Object findByfirstname(String name);
    public long addVolunteerToVolunteersTable(VolunteerForm volunteer);
    public int addVolunteerToUsersTable(VolunteerForm volunteer);
    public List<VolunteerDTO> findAllSummary();

    public boolean isEmailIdExists(String email);

    //public Object findUserLog(long id);
    List<VolunteerLogDTO> findAllLog(long id);
    public int addCredits(AddCreditsForm addCredits);
    public int spendCredits(SpendCreditsForm spendCredits);
}


