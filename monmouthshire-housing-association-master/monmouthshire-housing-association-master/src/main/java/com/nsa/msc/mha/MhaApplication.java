package com.nsa.msc.mha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MhaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MhaApplication.class, args);
    }

}
