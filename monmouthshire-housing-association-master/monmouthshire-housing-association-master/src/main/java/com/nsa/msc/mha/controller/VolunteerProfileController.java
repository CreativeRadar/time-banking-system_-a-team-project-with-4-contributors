package com.nsa.msc.mha.controller;
import com.nsa.msc.mha.dto.VolunteerDTO;
import com.nsa.msc.mha.dto.VolunteerLogDTO;
import com.nsa.msc.mha.repository.VolunteerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Controller
public class VolunteerProfileController {

    @Autowired
    private VolunteerRepository volunteerRepo;

    public VolunteerProfileController(VolunteerRepository pRepo) {
        volunteerRepo = pRepo;
    }

    @GetMapping("/admin/volunteer/{object}")
    public ModelAndView Greeting(@PathVariable long object){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("volunteerHome");
        Map model = mv.getModel();
        VolunteerDTO volunteer;
        volunteer = (VolunteerDTO) volunteerRepo.findById(object);
        model.put("object", volunteer);
        model.put("isHome", false);

        List<VolunteerLogDTO> volunteerLog;
        volunteerLog = volunteerRepo.findAllLog(object);
        model.put("logs" , volunteerLog);
        return mv;
    }

    @GetMapping("/volunteerHome/{id}")
    public ModelAndView Home(@PathVariable Long id){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("volunteerHome");
        Map model = mv.getModel();
        VolunteerDTO volunteer;
        volunteer = (VolunteerDTO)volunteerRepo.findById(id);
        model.put("volunteer" , volunteer);
        model.put("isHome" , true);

        List<VolunteerLogDTO> volunteerLog;
        volunteerLog = volunteerRepo.findAllLog(id);
        model.put("logs" , volunteerLog);
        return mv;
    }

}
