package com.nsa.msc.mha.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VolunteerDTO {
    private Long id;
    private Long time_credits;
    private Long earned_time_credits;
    private String phone_number;
    private String firstname;
    private String lastname;
    private String email;
    private String address_line_one;
    private String address_line_two;
    private String address_line_three;
    private String postcode;
    private String profile_image;
    private String barcode_image;
    private String role;
    private String password;

    private boolean is_mha_tenant;
    private String emergency_contact_number;
    private boolean previously_volunteered;
    private String previously_volunteered_who_for_and_role;
    private boolean disability_or_support_needs;
    private String disability_or_support_needs_details;
    private boolean medical_conditions_or_medications;
    private String medical_conditions_or_medications_details;
    private boolean known_allergies;
    private String known_allergies_details;

    public VolunteerDTO() {
    }

    ;

    public VolunteerDTO(Long id, Long time_credits, Long earned_time_credits, String phone_number, String firstname, String lastname, String email, String address_line_one, String postcode, String profile_image, String barcode_image, String role, String password) {
        this.id = id;
        this.time_credits = time_credits;
        this.earned_time_credits = earned_time_credits;
        this.phone_number = phone_number;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.address_line_one = address_line_one;
        this.postcode = postcode;
        this.profile_image = profile_image;
        this.barcode_image = barcode_image;
        this.role = role;
        this.password = password;

    }

    // Constructor for dashboard summary request
    public VolunteerDTO(Long id, Long time_credits, String firstname, String lastname) {
        this.id = id;
        this.time_credits = time_credits;
        this.firstname = firstname;
        this.lastname = lastname;
    }
    // Constructor for checking for enough credits
    public VolunteerDTO(Long id, Long time_credits) {
        this.id = id;
        this.time_credits = time_credits;
    }

    public Long getid() {
        return id;
    }

    public Long gettimecredits() {
        return time_credits;
    }

    public String getfirstname() {
        return firstname;
    }

    public String getfullname() {
        return firstname + " " + lastname;
    }

    public String getaddress_line_one() {
        return address_line_one;
    }

    public String getpostcode() {
        return postcode;
    }

    public String getphone_number() {
        return "Tel: " + phone_number;
    }

    public String getprofileimage() {
        return profile_image;
    }

    public String getbarcodeimage() {
        return barcode_image;
    }

    public String getemail() {
        return "Email: " + email;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public String getUrl() {
        return "/admin/volunteer/" + id;
    }

    public String getbadge() {

        if (earned_time_credits < 10) {
            return "/image/bronze-badge.png";
        }

        if (earned_time_credits < 20 && earned_time_credits >=10) {
            return "/image/Silver-Badge.png";
        }
        if (earned_time_credits >= 20) {
            return "/image/Gold-Badge.png";
        }

        else{return "/image/bronze-badge.png"; }



    }
}
