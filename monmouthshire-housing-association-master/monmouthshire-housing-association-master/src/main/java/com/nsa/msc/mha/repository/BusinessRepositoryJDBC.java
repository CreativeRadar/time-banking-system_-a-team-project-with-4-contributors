package com.nsa.msc.mha.repository;

import com.nsa.msc.mha.Forms.BusinessForm;
import com.nsa.msc.mha.dto.BusinessDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BusinessRepositoryJDBC implements BusinessRepository {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public BusinessRepositoryJDBC(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public List<BusinessDTO> findAll() {
        return jdbcTemplate.query("SELECT id, name FROM businesses", new Object[]{},
                (rs, i) -> new BusinessDTO(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("summary"),
                        rs.getString("address"),
                        rs.getString("logoURL")
                ));

    }

    @Override
    public boolean delete(long id) {
        return (jdbcTemplate.update("delete from businesses where id =(?);", id) == 1);
    }

    @Override
    public long addBusinessToBusinessesTable (BusinessForm business){
        return jdbcTemplate.update("insert into businesses(name, main_contact_name, main_contact_number, " +
                        "about_business, find_out_about_time_exchange, why_join_time_exchange, organisation_offers) " +
                        "values(?,?,?,?,?,?,?)",
                new Object[]{business.getName(),business.getMain_contact_name(),business.getMain_contact_number(),
                        business.getAbout_business(), business.getFind_out_about_time_exchange(),business.getWhy_join_time_exchange(),
                        business.getOrganisation_offers()});
    }

    @Override
    public int addBusinessToUsersTable (BusinessForm business){
        return jdbcTemplate.update("insert into users(email, role, business_id, address_line_one, address_line_two, " +
                        "address_line_three, postcode) values(?,'business',?,?,?,?,?)",
                new Object[]{business.getEmail(),jdbcTemplate.queryForObject("select max(id) from businesses",Long.class),
                        business.getAddress_line_one(), business.getAddress_line_two(), business.getAddress_line_three(),
                        business.getPostcode()});
    }

}

