package com.nsa.msc.mha.utilities;

public interface EmailService {

    void sendSimpleMessage(String to, String subject, String text);


    }
