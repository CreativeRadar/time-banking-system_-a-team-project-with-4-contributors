package com.nsa.msc.mha.controller.api;

import com.nsa.msc.mha.Forms.AddCreditsForm;
import com.nsa.msc.mha.Forms.SpendCreditsForm;
import com.nsa.msc.mha.repository.BusinessRepository;
import com.nsa.msc.mha.repository.VolunteerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class VolunteerRESTController {

    private VolunteerRepository volunteerRepo;

    @Autowired
    public VolunteerRESTController(VolunteerRepository volunteerRepo) {
        this.volunteerRepo = volunteerRepo;
    }

    @RequestMapping(path="/user/add_credits/")
    @ResponseBody
    public String addCredits(AddCreditsForm addCreditsForm){
        int result = volunteerRepo.addCredits(addCreditsForm);

        if (result == -1) {
            return "{\"success\":0, \"message\":\"This operation would take the volunteer over 12 credits this week.\"}";
        }

        if (result == 1) {
            return "{\"success\":1}";
        }

        return "{\"success\":0, \"message\":\"Something went wrong.\"}";
    }

    @RequestMapping(path = "/getVolunteerEmail/{email}", method = RequestMethod.GET)
    public boolean volunteerEmailExists(@PathVariable String email) {
        return volunteerRepo.isEmailIdExists(email);
    }

    @RequestMapping(path="/user/spend_credits/")
    @ResponseBody
    public String spend_credits(SpendCreditsForm spendCreditsForm){
        int repoResult = volunteerRepo.spendCredits(spendCreditsForm);

        // TODO: at least do a switch
        if (repoResult == -1) {
            return "{\"success\":0, \"message\":\"The user cannot be verified.\"}";
        }

        if (repoResult == -2) {
            return "{\"success\":0, \"message\":\"The user doesn't have enough credits for this operation.\"}";
        }

        if (repoResult == 1) {
            return "{\"success\":1}";
        }

        return "{\"success\":0, \"message\":\"Something went wrong.\"}";
    }
}

