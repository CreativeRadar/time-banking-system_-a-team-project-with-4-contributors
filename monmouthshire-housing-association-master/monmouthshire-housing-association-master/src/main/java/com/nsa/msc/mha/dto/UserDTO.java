package com.nsa.msc.mha.dto;
import org.springframework.security.crypto.bcrypt.BCrypt;

// Adapted from "Salted Password Hashing - Doing it Right"
// https://crackstation.net/hashing-security.htm

public class UserDTO {
    private long id;
    private String password;
    private String salt;
    private String role;

    public UserDTO(long id, String password, String role) {
        this.id = id;
        this.password = password;
        this.role = role;
    }

    public UserDTO() {
    };

    public void setEncryptedPassword(String plainPassword) {
        this.salt = BCrypt.gensalt();
        this.password = BCrypt.hashpw(plainPassword, this.salt);
    }

    public boolean verifyPassword(String candidatePassword) {
        // return BCrypt.checkpw(candidatePassword, this.password);
        return this.password.equals(BCrypt.hashpw(candidatePassword, this.salt));
    }

    public long getId() {
        return id;
    }
    public String getPassword() {
        return password;
    }
    public String getRole() {
        return role;
    }
}
