package com.nsa.msc.mha.model;

import com.nsa.msc.mha.dto.VolunteerLogDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VolunteerLogRowMapper implements RowMapper<VolunteerLogDTO> {
    @Override
    public VolunteerLogDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new VolunteerLogDTO(

                rs.getLong("user_id"),
                rs.getString("task_description"),
                rs.getString("credits_earned"),
                rs.getString("timestamp"),
                rs.getString("location")


        );


    }
}
