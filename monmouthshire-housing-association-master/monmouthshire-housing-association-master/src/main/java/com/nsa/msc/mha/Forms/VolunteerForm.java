package com.nsa.msc.mha.Forms;

public class VolunteerForm {
    private String email;
    private String firstname;
    private String lastname;
    private String address_line_one;
    private String address_line_two;
    private String address_line_three;
    private String postcode;
    private boolean is_mha_tenant;
    private String phone_number;
    private String emergency_contact_number;
    private boolean previously_volunteered;
    private String previously_volunteered_who_for_and_role;
    private boolean disability_or_support_needs;
    private String disability_or_support_needs_details;
    private boolean medical_conditions_or_medications;
    private String medical_conditions_or_medications_details;
    private boolean known_allergies;
    private String known_allergies_details;

    public VolunteerForm(String email, String firstname, String lastname, String address_line_one, String address_line_two,
                         String address_line_three, String postcode, boolean is_mha_tenant, String phone_number,
                         String emergency_contact_number, boolean previously_volunteered, String previously_volunteered_who_for_and_role,
                         boolean disability_or_support_needs, String disability_or_support_needs_details,
                         boolean medical_conditions_or_medications, String medical_conditions_or_medications_details,
                         boolean known_allergies, String known_allergies_details) {
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address_line_one = address_line_one;
        this.address_line_two = address_line_two;
        this.address_line_three = address_line_three;
        this.postcode = postcode;
        this.is_mha_tenant = is_mha_tenant;
        this.phone_number = phone_number;
        this.emergency_contact_number = emergency_contact_number;
        this.previously_volunteered = previously_volunteered;
        this.previously_volunteered_who_for_and_role = previously_volunteered_who_for_and_role;
        this.disability_or_support_needs = disability_or_support_needs;
        this.disability_or_support_needs_details = disability_or_support_needs_details;
        this.medical_conditions_or_medications = medical_conditions_or_medications;
        this.medical_conditions_or_medications_details = medical_conditions_or_medications_details;
        this.known_allergies = known_allergies;
        this.known_allergies_details = known_allergies_details;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAddress_line_one() {
        return address_line_one;
    }

    public String getAddress_line_two() {
        return address_line_two;
    }

    public String getAddress_line_three() {
        return address_line_three;
    }

    public String getPostcode() {
        return postcode;
    }

    public boolean isIs_mha_tenant() {
        return is_mha_tenant;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getEmergency_contact_number() {
        return emergency_contact_number;
    }

    public boolean isPreviously_volunteered() {
        return previously_volunteered;
    }

    public String getPreviously_volunteered_who_for_and_role() {
        return previously_volunteered_who_for_and_role;
    }

    public boolean isDisability_or_support_needs() {
        return disability_or_support_needs;
    }

    public String getDisability_or_support_needs_details() {
        return disability_or_support_needs_details;
    }

    public boolean isMedical_conditions_or_medications() {
        return medical_conditions_or_medications;
    }

    public String getMedical_conditions_or_medications_details() {
        return medical_conditions_or_medications_details;
    }

    public boolean isKnown_allergies() {
        return known_allergies;
    }

    public String getKnown_allergies_details() {
        return known_allergies_details;
    }
}



