package com.nsa.msc.mha.repository;

import com.nsa.msc.mha.dto.UserDTO;

public interface UserRepository {
    public UserDTO findByEmail(String email);
}
