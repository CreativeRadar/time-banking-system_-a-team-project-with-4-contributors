package com.nsa.msc.mha.Forms;

public class BusinessForm {
    private String name;
    private String email;
    private String address_line_one;
    private String address_line_two;
    private String address_line_three;
    private String postcode;
    private String main_contact_name;
    private String main_contact_number;
    private String about_business;
    private String find_out_about_time_exchange;
    private String why_join_time_exchange;
    private String organisation_offers;

    public BusinessForm(String name, String email, String address_line_one, String address_line_two, String address_line_three,
                        String postcode, String main_contact_name, String main_contact_number, String about_business,
                        String find_out_about_time_exchange, String why_join_time_exchange, String organisation_offers) {
        this.name = name;
        this.email = email;
        this.address_line_one = address_line_one;
        this.address_line_two = address_line_two;
        this.address_line_three = address_line_three;
        this.postcode = postcode;
        this.main_contact_name = main_contact_name;
        this.main_contact_number = main_contact_number;
        this.about_business = about_business;
        this.find_out_about_time_exchange = find_out_about_time_exchange;
        this.why_join_time_exchange = why_join_time_exchange;
        this.organisation_offers = organisation_offers;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress_line_one() {
        return address_line_one;
    }

    public String getAddress_line_two() {
        return address_line_two;
    }

    public String getAddress_line_three() {
        return address_line_three;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getMain_contact_name() {
        return main_contact_name;
    }

    public String getMain_contact_number() {
        return main_contact_number;
    }

    public String getAbout_business() {
        return about_business;
    }

    public String getFind_out_about_time_exchange() {
        return find_out_about_time_exchange;
    }

    public String getWhy_join_time_exchange() {
        return why_join_time_exchange;
    }

    public String getOrganisation_offers() {
        return organisation_offers;
    }
}
