
package com.nsa.msc.mha.controller;

import com.nsa.msc.mha.dto.BusinessDTO;
import com.nsa.msc.mha.dto.VolunteerDTO;
import com.nsa.msc.mha.repository.BusinessRepository;
import com.nsa.msc.mha.repository.VolunteerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
@Controller
public class DashboardController {

    private BusinessRepository businessRepo;
    private VolunteerRepository volunteerRepo;

    @Autowired
    public DashboardController(BusinessRepository businessRepo, VolunteerRepository volunteerRepo) {
        this.businessRepo = businessRepo;
        this.volunteerRepo = volunteerRepo;
    }


    @RequestMapping(path = "admin")
    public String getDashboard(Model model) {
        List<VolunteerDTO> volunteers = volunteerRepo.findAllSummary();
        List<BusinessDTO> businesses = businessRepo.findAll();

        model.addAttribute("volunteers", volunteers);
        model.addAttribute("businesses", businesses);
        return "dashboard";
    }

}
