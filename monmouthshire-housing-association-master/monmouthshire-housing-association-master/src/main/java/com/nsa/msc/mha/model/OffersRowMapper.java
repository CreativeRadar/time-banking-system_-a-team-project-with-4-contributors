package com.nsa.msc.mha.model;
import com.nsa.msc.mha.dto.OfferDTO;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OffersRowMapper implements RowMapper<OfferDTO> {
        @Override
        public OfferDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new OfferDTO(
                    rs.getInt("offer_id"),
                    rs.getString("offer"),
                    rs.getString("business_name")
            );

        }
    }