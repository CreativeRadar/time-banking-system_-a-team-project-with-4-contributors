package com.nsa.msc.mha.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class VolunteerLogDTO {

    private Long user_id;
    private String task_description;
    private String credits_earned;
    private String timestamp;
    private String location;

    public VolunteerLogDTO(){};


    public VolunteerLogDTO(Long user_id, String task_description, String credits_earned, String timestamp, String location) {
        this.user_id = user_id;
        this.task_description = task_description;
        this.credits_earned = credits_earned;
        this.timestamp = timestamp;
        this.location = location;
    }

    public Long getUser_id() {
        return user_id;
    }

    public String getTask_description() {
        return task_description;
    }

    public String getCredits_earned() {
        return credits_earned;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getLocation() {
        return location;
    }
}


