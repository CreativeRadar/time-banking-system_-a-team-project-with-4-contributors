package com.nsa.msc.mha.Forms;

public class SpendCreditsForm {
    private long userCode;
    private long offerId;

    public SpendCreditsForm(long user_code, long offer_id) {
        this.userCode = user_code;
        this.offerId = offer_id;
    }

    public long getUserCode() {
        return userCode;
    }

    public long getOfferId() {
        return offerId;
    }
}
