package com.nsa.msc.mha.controller;

import com.nsa.msc.mha.repository.BusinessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class BusinessRESTController {

    private BusinessRepository businessRepo;

    @Autowired
    public BusinessRESTController(BusinessRepository businessRepo) {
        this.businessRepo = businessRepo;
    }

    @RequestMapping(path="/business/delete/{id}")
    public String deleteBusiness(@PathVariable Optional<String> id){
        if(!id.isPresent()) {
            return "No ID provided";
        }
        try {
            int intId = Integer.parseInt(id.orElse(""));
            return (businessRepo.delete(intId) ? "Item deleted" : "Item not deleted");
        } catch (Exception e){
            e.printStackTrace();
            return "Invalid ID";
        }
    }
}

