package com.nsa.msc.mha.repository;

import com.nsa.msc.mha.Forms.SpendCreditsForm;
import com.nsa.msc.mha.Forms.logInForm;
import com.nsa.msc.mha.Forms.AddCreditsForm;
import com.nsa.msc.mha.dto.OfferDTO;
import com.nsa.msc.mha.dto.VolunteerDTO;
import com.nsa.msc.mha.dto.VolunteerLogDTO;
import com.nsa.msc.mha.model.VolunteerRowMapper;
import com.nsa.msc.mha.model.VolunteerLogRowMapper;
import com.nsa.msc.mha.Forms.VolunteerForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VolunteerRepositoryJDBC implements VolunteerRepository {

    private JdbcTemplate jdbcTemplate;


    @Autowired
    public VolunteerRepositoryJDBC(JdbcTemplate aTemplate) {
        jdbcTemplate = aTemplate;
    }

    // use CustomRowMapper
//    @Override
//    public List<VolunteerDTO> findAll() {
//        VolunteerRowMapper erm = new VolunteerRowMapper();
//        return jdbcTemplate.query("select id, firstname, lastname, address_line_one, postcode, email, phone_number, time_credits, profile_image, barcode_image from volunteers", erm);
//    }

    // Summary for dashboard
    @Override
    public List<VolunteerDTO> findAllSummary() {
           return jdbcTemplate.query("SELECT u.id,\n" +
                        "  COALESCE(SUM(t.credits_earned),0) - COALESCE(SUM(o.credits),0) as time_credits,\n" +
                        "  v.firstname,\n" +
                        "  v.lastname\n" +
                        "FROM users AS u\n" +
                        "JOIN volunteers as v on u.volunteer_id = v.id\n" +
                        "LEFT JOIN transactions as t on u.id = t.user_id\n" +
                        "LEFT JOIN offers as o on t.offer_id = o.offer_id\n" +
                        "GROUP BY u.id ", new Object[]{},
                (rs, i) -> new VolunteerDTO(
                        rs.getLong("id"),
                        rs.getLong("time_credits"),
                        rs.getString("firstname"),
                        rs.getString("lastname")
                ));
    }

    @Override
    public VolunteerDTO findById(Long id) {
        VolunteerRowMapper erm = new VolunteerRowMapper();
        return jdbcTemplate.queryForObject("SELECT users.id, volunteers.firstname,  volunteers.lastname, users.address_line_one, users.postcode, users.email, users.phone_number, users.time_credits, COALESCE(SUM(transactions.credits_earned),0)  as earned_time_credits, users.profile_image, users.barcode_image, users.role, users.password from users\n" +
                        "JOIN volunteers ON users.volunteer_id = volunteers.id\n" +
                        "LEFT JOIN transactions on users.id = transactions.user_id\n" +
                        "WHERE users.id =  ?",
                new Object[]{id},
                erm);
    }

    @Override
    public int addCredits(AddCreditsForm addCredits){
        // Fixed using https://www.mkyong.com/spring/queryforobject-throws-emptyresultdataaccessexception-when-record-not-found/

        int creditsThisWeek;

        try {
            creditsThisWeek = jdbcTemplate.queryForObject("SELECT SUM(t.credits_earned) AS earned_this_week FROM transactions AS t WHERE t.user_id = ? AND t.timestamp >= DATE_SUB(CURRENT_DATE(), INTERVAL DAYOFWEEK(CURRENT_DATE())-2 DAY)",
                    new Object[]{addCredits.getId()}, Integer.class);
        } catch (Exception e) {
            creditsThisWeek = 0;
        }

        if (creditsThisWeek + addCredits.getCredits() > 12) {
            return -1; // can't earn more
        }

        jdbcTemplate.update("UPDATE users SET time_credits = time_credits + ? WHERE (id = ?);",
                new Object[]{
                        addCredits.getCredits(),
                        addCredits.getId()
                });

        // TODO: update user.time_credits as well
        return jdbcTemplate.update("INSERT INTO transactions (user_id, type, task_description, credits_earned, location) VALUES (?, '1', ?, ?, ?);",
                new Object[]{
                        addCredits.getId(),
                        addCredits.getDescription(),
                        addCredits.getCredits(),
                        addCredits.getLocation()
                });
    }

    @Override
    public int spendCredits(SpendCreditsForm spendCredits){
        // TODO: rethink return strategy
        // 1 means one row altered, 0 no rows altered, -1 user doesn't exist, -2 not enough credits

        VolunteerDTO validatedUser = jdbcTemplate.queryForObject("SELECT u.id, COALESCE(SUM(t.credits_earned),0) - COALESCE(SUM(o.credits),0) as time_credits\n" +
                "FROM users AS u\n" +
                "LEFT JOIN transactions as t on u.id = t.user_id\n" +
                "LEFT JOIN offers as o on t.offer_id = o.offer_id\n" +
                "WHERE u.id = ? AND u.role = \"volunteer\"", new Object[]{spendCredits.getUserCode()},
                (rs, i) -> new VolunteerDTO(
                        rs.getLong("id"),
                        rs.getLong("time_credits")));

        if (validatedUser.getid() == 0) {
            return -1; // user doesnt exist
        }

        OfferDTO offerCandidate = jdbcTemplate.queryForObject("SELECT o.offer_id, o.credits FROM offers as o WHERE o.offer_id = ?", new Object[]{spendCredits.getOfferId()},
                (rs, i) -> new OfferDTO(
                        rs.getInt("offer_id"),
                        rs.getInt("credits")));


        if (validatedUser.gettimecredits() < offerCandidate.getCredits()) {
            return -2; // user doesn't have enough credits
        }

        jdbcTemplate.update("UPDATE users SET time_credits = time_credits - ? WHERE (id = ?);",
                new Object[]{
                        offerCandidate.getCredits(),
                        validatedUser.getid()
                });

        return jdbcTemplate.update("INSERT INTO transactions (user_id, type, offer_id) VALUES (?, '0', ?);",
                new Object[]{ spendCredits.getUserCode(), spendCredits.getOfferId() });

    }

//
//    @Override
//    public VolunteerDTO findByfirstname(String name) {
//        VolunteerRowMapper erm = new VolunteerRowMapper();
//        return jdbcTemplate.queryForObject("select id, firstname, lastname, address_line_one, postcode, email, phone_number, time_credits, profile_image, barcode_image from volunteers where firstname = ?",
//                new Object[]{name},
//                erm);
//    }

    @Override
    public long addVolunteerToVolunteersTable(VolunteerForm volunteer) {
        return  jdbcTemplate.update("insert into volunteers(firstname, lastname, is_mha_tenant, emergency_contact_number, " +
                        "previously_volunteered, previously_volunteered_who_for_and_role, disability_or_support_needs," +
                        "disability_or_support_needs_details, medical_conditions_or_medications, " +
                        "medical_conditions_or_medications_details, known_allergies, known_allergies_details)" +
                        " values(?,?,?,?,?,?,?,?,?,?,?,?)",
                new Object[]{volunteer.getFirstname(), volunteer.getLastname(), volunteer.isIs_mha_tenant(),
                        volunteer.getEmergency_contact_number(), volunteer.isPreviously_volunteered(),
                        volunteer.getPreviously_volunteered_who_for_and_role(), volunteer.isDisability_or_support_needs(),
                        volunteer.getDisability_or_support_needs_details(), volunteer.isMedical_conditions_or_medications(),
                        volunteer.getMedical_conditions_or_medications_details(), volunteer.isKnown_allergies(),
                        volunteer.getKnown_allergies_details()});
    }

    @Override
    public int addVolunteerToUsersTable (VolunteerForm volunteer) {
        return jdbcTemplate.update("insert into users(email, role, address_line_one, address_line_two, address_line_three, " +
                        "postcode, phone_number, volunteer_id) values(?,'volunteer', ?,?,?,?,?,?)",
                new Object[]{volunteer.getEmail(), volunteer.getAddress_line_one(), volunteer.getAddress_line_two(),
                        volunteer.getAddress_line_three(),volunteer.getPostcode(),volunteer.getPhone_number(),
                        jdbcTemplate.queryForObject("select max(id) from volunteers",Long.class)}); //(Wawer, D. 2016)

    }

    @Override
    public VolunteerDTO findByEmail(String email) {
        VolunteerRowMapper erm = new VolunteerRowMapper();
        return jdbcTemplate.queryForObject("select users.id, volunteers.firstname, volunteers.lastname, users.address_line_one, users.postcode, users.email, users.phone_number, users.time_credits, users.profile_image, users.barcode_image, users.role, users.password from users JOIN volunteers ON users.volunteer_id = volunteers.id WHERE users.email = ?",
                new Object[]{email},
                erm);
    }


    @Override
    public boolean isEmailIdExists(String email) {
        boolean emailExists = false;
        int count = jdbcTemplate.queryForObject("SELECT count(*) FROM users WHERE email = ?",
                new Object[] {email}, Integer.class);
        if (count > 0)
            emailExists = true;
        return emailExists;
    }

//    @Override
//    public VolunteerLogDTO findUserLog(long id) {
//        VolunteerLogRowMapper erm = new VolunteerLogRowMapper();
//        return jdbcTemplate.queryForObject("SELECT user_id, task_description, credits_earned, timestamp, location FROM transactions WHERE user_id = ? AND type =1",
//                new Object[]{id},
//                erm);
//    }

    @Override
    public List<VolunteerLogDTO> findAllLog(long id) {
        VolunteerLogRowMapper erm = new VolunteerLogRowMapper();
        return jdbcTemplate.query("SELECT user_id, task_description, credits_earned, timestamp, location FROM transactions WHERE user_id = ? AND type =1",
                new Object[]{id},
                erm);
    }


}

/* Reference:
 * Wawer, D. 2016. Available at:
 * https://stackoverflow.com/questions/15661313/jdbctemplate-queryforint-long-is-deprecated-in-spring-3-2-2-what-should-it-be-r
 * [Accessed: 08 December 2018].  I have adapted the code to return the volunteer id that is generated when the volunteer
 * registration form is submitted. */
