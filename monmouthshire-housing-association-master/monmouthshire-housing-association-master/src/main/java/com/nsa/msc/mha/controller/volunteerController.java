package com.nsa.msc.mha.controller;

import com.nsa.msc.mha.Forms.VolunteerForm;
import com.nsa.msc.mha.repository.VolunteerRepository;
import com.nsa.msc.mha.utilities.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;


// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)

    @Controller
    public class volunteerController {

        private VolunteerRepository volunteerRepo;
        private EmailService emailSvc;

        @Autowired
        public volunteerController(VolunteerRepository volunteerRepository, EmailService aSvc){
            volunteerRepo = volunteerRepository;
            emailSvc=aSvc;
        }


        @PostMapping(path={"/registrationConfirmationVolunteer"})
                public String registrationConfirmation (VolunteerForm volunteerForm){
                  volunteerRepo.addVolunteerToVolunteersTable(volunteerForm);
                  volunteerRepo.addVolunteerToUsersTable(volunteerForm);

                  //call emailService to send the email
            String volunteerDetails = "\nFirstname: " + volunteerForm.getFirstname() +"\nLastname: "
                    + volunteerForm.getLastname()+ "\n MHA Tenant: "+volunteerForm.isIs_mha_tenant()+"\n1st Line of Address: "
                    + volunteerForm.getAddress_line_one()+"\nPhone number"
                    + volunteerForm.getPhone_number()+"\nEmergency Phone Number: "+volunteerForm.getEmergency_contact_number()+"\nEmail: "
                    +volunteerForm.getEmail()+"\nPreviously volunteered? "+ volunteerForm.isPreviously_volunteered() + "\nPreviously volunteered details "+
                    volunteerForm.getPreviously_volunteered_who_for_and_role() + "\n Has disabilities or needs support?"+ volunteerForm.isDisability_or_support_needs() +
                    "\n Details: " + volunteerForm.getDisability_or_support_needs_details() +"\nHas medical condition? "+volunteerForm.isMedical_conditions_or_medications()+
                    "\nDetails: " +volunteerForm.getMedical_conditions_or_medications_details() + "\nHas allergies: "+volunteerForm.isKnown_allergies() + "\n Details: " +
                    volunteerForm.getKnown_allergies_details();

                    emailSvc.sendSimpleMessage("danieljamesjimenez@outlook.com", "Volunteer Application", volunteerDetails);

                    emailSvc.sendSimpleMessage(volunteerForm.getEmail(), "MHA Application Confirmation", "Thank you for submitting your application form. \nMHA will seek to " +
                    "contact you shortly regarding your application. \nRegards,\nThe MHA Recruitment Team.");

            return "RegistrationConfirmation";
            }

            @GetMapping(path={"/volunteerRegistration"})
               public String volunteerRegistration(){
               return "RegistrationForm";
            }



    }


