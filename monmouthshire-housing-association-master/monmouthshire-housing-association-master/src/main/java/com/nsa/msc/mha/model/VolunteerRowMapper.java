package com.nsa.msc.mha.model;

import com.nsa.msc.mha.dto.VolunteerDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VolunteerRowMapper implements RowMapper<VolunteerDTO> {
    @Override
    public VolunteerDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new VolunteerDTO(
                        rs.getLong("id"),
                        rs.getLong("time_credits"),
                        rs.getLong("earned_time_credits"),
                        rs.getString("phone_number"),
                        rs.getString("firstname"),
                        rs.getString("lastname"),
                        rs.getString("email"),
                        rs.getString("address_line_one"),
                        rs.getString("postcode"),
                        rs.getString("profile_image"),
                        rs.getString("barcode_image"),
                        rs.getString("role"),
                        rs.getString("password")

        );

    }
}
