package com.nsa.msc.mha.dto;

// Comment out the lombok annotations or the build fails
// import lombok.AllArgsConstructor;
// import lombok.Data;

//@Data
//@AllArgsConstructor
public class OfferDTO {
    private int offer_id;
    private String offer;
    private int credits;
    private int business_id;
    private String business_name;

    //Added to fix errors when running the testing class BusinessFormTest
    public OfferDTO(int offer_id, String offer, String business_name) {
        this.offer_id = offer_id;
        this.offer = offer;
        this.business_name = business_name;
    }

    public OfferDTO(int offer_id, int credits) {
        this.offer_id = offer_id;
        this.credits = credits;
    }

    //Added to fix errors when running the testing class BusinessFormTest
    public int getOffer_id() {
        return offer_id;
    }

    //Added to fix errors when running the testing class BusinessFormTest
    public String getOffer() {
        return offer;
    }


    //Added to fix errors when running the testing class BusinessFormTest
    public String getBusiness_name() {
        return business_name;
    }

    public int getCredits() {
        return credits;
    }
}
