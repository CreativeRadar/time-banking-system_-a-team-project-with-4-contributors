-- Get all transactions for a business
SELECT offers.offer, t.timestamp
  FROM transactions as t
  JOIN offers ON (offers.offer_id = t.offer_id)
  WHERE offers.business_id = 21


-- Get all transactions for a user
SELECT offers.offer, t.timestamp
  FROM transactions as t
  JOIN (offers, users) ON (offers.offer_id = t.offer_id and users.id = t.user_id)
  WHERE users.id = 5


-- Get credits earned, spent and balance for all users
SELECT t.user_id, SUM(t.credits_earned) AS total_earned, SUM(o.credits) AS total_spent, (SUM(t.credits_earned) - SUM(o.credits)) AS current_balance
FROM transactions AS t
LEFT JOIN offers AS o ON o.offer_id = t.offer_id


-- Get credits earned this week for a certain user
SELECT SUM(t.credits_earned) AS earned_this_week
FROM transactions AS t
WHERE t.user_id = 3 AND t.timestamp >= DATE_SUB(CURRENT_DATE(), INTERVAL DAYOFWEEK(CURRENT_DATE())-2 DAY)

-- Get user lists with the credit sum from transactions table (dis be crazy)
SELECT u.id AS uid,
  SUM(t.credits_earned),
  SUM(o.credits),
  COALESCE(SUM(t.credits_earned),0) - COALESCE(SUM(o.credits),0) as time_credits,
  v.firstname,
  v.lastname
FROM users AS u
LEFT JOIN volunteers as v on u.volunteer_id = v.id
LEFT JOIN transactions as t on u.id = t.user_id
LEFT JOIN offers as o on t.offer_id = o.offer_id
GROUP BY u.id

-- Date experiments
SELECT t.user_id, SUM(t.credits_earned) AS total_earned,
  DATE_SUB(CURRENT_DATE(), INTERVAL DAYOFWEEK(CURRENT_DATE())-2 DAY) as monday,
  DAYOFWEEK(CURRENT_DATE()) as dayofweek,
  CURRENT_DATE() as today,
  DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY) as yesterday
FROM transactions AS t
WHERE t.user_id = 3
